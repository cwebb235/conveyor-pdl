# conveyor-pdl

This project process raw conveyor belt data from BHP into cleaned, anonymised data hosted on the Prognostics Data Library (PDL).

It also builds a learning table from these data to support the development of predictive models.

## Repository structure

The respository is partitioned into three primary directories:

* data — space for raw, unprocessed data, clean output data, and intermediate files generated during the data preparation process.
* R — R code for processing the data and performing modelling.
* sql — sql procedures for processing the data, which are reproduced in R code.

## Data

To minimise the size of the repository, the raw data from BHP is hosted externally.

Download the data from the following Google drive link and unzip its contents to the *data* folder:

https://drive.google.com/drive/folders/1W6-W2B5lprkLhHX5QRZE3lZ6fdQa35B-?usp=sharing

This will create five subfolders:

* belt-con-mon — Conveyor belt condition monitoring reports, in spreadsheet format.
* belt-grade — Contains a lookup table spreadsheet for inferring conveyor belt material. 
* conveyor-spec — Contains a conveyor belt system specification lookup table spreadsheet.
* material-tracking — Movements of iron ore through the BHP supply chain.
* output — Initially empty, but will contain anonymised PDL data, anonymisation maps (private), and modelling data.

## Producing output data

To execute the data preparation process and rebuild the PDL data, run the code found in *R/notebooks/pdl-data-preparation.Rmd*. 

To rebuild the learning table based on the PDL data, run the code found in *modelling-data-preparation.Rmd*.

## Reproducing modelling results

Modelling is performed in *R/notebooks/modelling.Rmd* and requires the learning table *model-data.rds* to be built and present in *data/output/*

