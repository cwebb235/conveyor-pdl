USE [ConveyorData]
GO
/****** Object:  StoredProcedure [dbo].[DetermineTonnages3]    Script Date: 15/09/2018 2:31:10 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[DetermineTonnages3]
AS
BEGIN
	SET NOCOUNT ON;
	update v_WearMeasDates 
	set	
	lumpMT = dbo.GetPastMegaTonnes(conveyor_id_anon,'Lump',testdate,firstdate,0)*1.00,
	finesMT = dbo.GetPastMegaTonnes(conveyor_id_anon,'Fines',testdate,firstdate,0)*1.00,
	lumpFiltMT = dbo.GetPastMegaTonnes(conveyor_id_anon,'Lump',testdate,firstdate,1)*1.00,
	finesFiltMT = dbo.GetPastMegaTonnes(conveyor_id_anon,'Fines',testdate,firstdate,1)*1.00,
	op_hours=datediff(minute,firstdate,testdate)/60.0,
	processed=1
	
	
END




GO
