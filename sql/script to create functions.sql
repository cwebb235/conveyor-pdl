USE [ConveyorData]
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetLengthClass]    Script Date: 15/09/2018 2:31:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[fn_GetLengthClass]
(@length integer
)
RETURNS varchar(10)
AS
BEGIN
	declare @lc varchar(10);

	select @LC= CASE 
		WHEN @length > 0 and @length <= 300 THEN '0-300'
		WHEN @length > 300 and @length <= 1000 THEN '300-1000'
		WHEN @length > 1000 and @length <= 1500 THEN '1000-1500'
		WHEN @length > 1500 and @length <= 2200 THEN '1500-2200'
		WHEN @length > 2200 and @length <=2750 THEN '2200-2750'
		WHEN @length > 2750 THEN '2800+'
		else 'Unknown'
	  END

	return @LC;

END
GO
/****** Object:  UserDefinedFunction [dbo].[fn_GetWidthClass]    Script Date: 15/09/2018 2:31:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[fn_GetWidthClass]
(@width integer
)
RETURNS varchar(6)
AS
BEGIN
	declare @wc varchar(6);

	select @WC= CASE 
		WHEN @width > 1100 and @width < 1250 THEN 'CW1200'
		WHEN @width > 1400 and @width < 1505 THEN 'CW1500'
		WHEN @width > 1505 and @width < 1605 THEN 'CW1600'
		WHEN @width > 1605 and @width < 1805 THEN 'CW1800'
		WHEN @width > 1805 and @width < 2005 THEN 'CW2000'
		WHEN @width > 2050 and @width < 2250 THEN 'CW2200'
		else 'UNKNWN'
	  END

	return @wc;

END
GO
/****** Object:  UserDefinedFunction [dbo].[GetPastMegaTonnes]    Script Date: 15/09/2018 2:31:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetPastMegaTonnes] 
(
	@conv_id nvarchar(50),
	@product_type nvarchar(50),
	@testdate datetime,
	@tonnes_start datetime,
	@dofilter bit
)
RETURNS float
AS
BEGIN
	DECLARE 
	@totaltonnes float,
	@totalhrs real;
	

	set @totalhrs = datediff(minute, @tonnes_start, @testdate)/60.0;
		
	if @totalhrs <= 0.0 set @totaltonnes=0.0; 

	if @totalhrs > 0.0  
		if @dofilter=0  /***  No filtering on tonnage  ***/
			select @totaltonnes = 
				sum(c.tonnes)/1.0e6 
					from v_conveyorMovements c
					where 
						c.product_group = @product_type 
						and datediff(minute,[start_datetime],[end_datetime])/60.0 > 0.0000
						and c.conveyor_id_anon=@conv_id
						and c.start_datetime > @tonnes_start
						and c.end_datetime < @testdate;
	
	
		else /***  Filter on tonnage  ***/
			select @totaltonnes = 
				sum(c.tonnes)/1.0e6
				from v_conveyorMovements c
					where 
					c.product_group = @product_type 
					and datediff(minute,[start_datetime],[end_datetime])/60.0 > 0.0000
					and c.conveyor_id_anon=@conv_id
					and c.start_datetime > @tonnes_start
					and c.end_datetime < @testdate
					and (c.tonnes)/(datediff(minute,[start_datetime],[end_datetime])/60.0) > 300.0
					and (c.tonnes)/(datediff(minute,[start_datetime],[end_datetime])/60.0) < 15000.0;

	
	/*** if @totaltonnes is null set @totaltonnes=0;  ***/
	

	RETURN @totaltonnes;

END


GO
/****** Object:  UserDefinedFunction [dbo].[f_GetReportValues]    Script Date: 15/09/2018 2:31:51 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Joanna Sikorska
-- Create date: <Create Date,,>
-- Description:	Extracts the set of values that will be used to determine the wear of the conveyor at that date.
-- =============================================
CREATE FUNCTION [dbo].[f_GetReportValues](@testdate datetime,	@report nvarchar(20))
RETURNS table 
as
return
	SELECT dbo.thickness_noNA.position, dbo.thickness_noNA.thickness_mm,date
			FROM dbo.thickness_noNA INNER JOIN
				dbo.report_attributes_noNA ON rtrim(ltrim(upper(dbo.thickness_noNA.report_id))) = rtrim(ltrim(upper(dbo.report_attributes_noNA.report_id)))
			WHERE rtrim(ltrim(upper(dbo.thickness_noNA.report_id))) = @report 
				AND (dbo.report_attributes_noNA.belt_width_mm - 400 - dbo.thickness_noNA.position > 0) 
				AND (dbo.thickness_noNA.position - 400 > 0) 
				AND (convert(varchar,dbo.thickness_noNA.date,103) = convert(varchar,@testdate,103));
	

GO
