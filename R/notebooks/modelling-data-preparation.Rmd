---
#
# IMPORTANT 
# This notebook is a little different to the others, in that 
# it includes a chunk that renders a child, pool-summary-template.Rmd
# in a loop. This code chunk will return an error if you attempt to run it directly,
# it is only meant to be run when knitting (through the Knit button or rmarkdown::render)
#
title: "Prepare Modelling Data"
output: 
  html_document:
    df_print: paged
    toc: true
    toc_float:
      collapsed: false
      smooth_scrool: true
---

<!-- This is a script to indent the table of contents based on header level -->
<script>
$(document).ready(function() {
  $items = $('div#TOC li');
  $items.each(function(idx) {
    num_ul = $(this).parentsUntil('#TOC').length;
    $(this).css({'text-indent': num_ul * 10, 'padding-left': 0});
  });

});
</script>

```{r setup, echo = FALSE, message = FALSE}
library(ggplot2)
library(cowplot)
library(knitr)
library(gridExtra)
library(stringr)

# Setting dplyr.show_progress to TRUE is useful for interactive use, but annoying when we want to knit to html, as it will
# produce a long chunk of useless output in the html document.
# set this to TRUE if you'd like to get feedback on how long complicated functions will take to finish during development.
# Caching speeds up knitting
options(dplyr.show_progress = FALSE)
knitr::opts_chunk$set(cache=TRUE) 

source("../data-prep/prepare-modelling-data.R")
```


## Purpose

This notebook steps through the process of taking the anonymised PDL data and creating a single learning table for the purpose of training a predictive model.

The `prepare-modelling-data.R` script contains functions that do most of the heavy lifting so that this notebook can be kep concise and conceptually high-level.

```{r}
# Root data directory
data_root <- "../../data/"

# Directory containing pdl data
pdl_dir <- paste0(data_root, "output/pdl/") 

# Directory to write modelling data when finished
model_dir <- paste0(data_root, "output/modelling/")

# create the output directory if it doesn't already exist
if (!dir.exists(file.path(model_dir))) {
  dir.create(model_dir)
}
```

## Data preparation

### Reading and cleaning

```{r}
rep_attr <- read.csv(paste0(pdl_dir, "report_attributes.csv"), stringsAsFactors = FALSE)
thickness <- read.csv(paste0(pdl_dir, "thickness.csv"), stringsAsFactors = FALSE)
util <- read.csv(paste0(pdl_dir, "utilisation.csv"), stringsAsFactors = FALSE)
```

Tidy data:

* Convert all dates to datetimes by assuming a time of 12:00 noon (UTC).
* Split material movement records over equipment ids, so that each row refers to a single piece of equipment.
* Remove material movements that don't involve a conveyor in our data. 
* Remove material movements that are below 300 tph or above 15000 tph, as they are unlikely to be valid.
* Remove thickness tests within 400mm of belt edges to exclude skirt wear.
* Remove thickness tests that predate any recorded movement for that equipment.

```{r}
rep_attr_tidy <- tidy_rep_attr(rep_attr)
util_tidy <- tidy_util(util, rep_attr_tidy)
thickness_tidy <- tidy_thickness(thickness, rep_attr_tidy, util_tidy)

# Write the tidy utilisation data to a file compatiable with SQL server (no NAs)
data.table::fwrite(util_tidy, file = paste0(data_root, "tidy_material_tracking_sql.csv"))
```

### Data reduction

Some conveyor duties have only a handful of observations, and are similar in design to other larger groups.
These will be merged:

* Merge Wharf and Tunnel duties into Yard
* Merge Car Dumper into Transfer

```{r}
rep_attr_tidy$conveyor_duty <- str_replace_all(rep_attr_tidy$conveyor_duty,
                                               "Wharf|Tunnel", "Yard")
rep_attr_tidy$conveyor_duty <- str_replace_all(rep_attr_tidy$conveyor_duty,
                                               "Car Dumper", "Transfer")
```

### Pooling

Pool thickness data into belt lifetime groups, retaining enough data from rep_attr_tidy to accumulate tonnage without performing more joins.

```{r}
thickness_pooled <- thickness_tidy %>%
  left_join(select(rep_attr_tidy, report_id, conveyor_id, eq_id, belt_install_datetime), 
            by = "report_id") %>%
  mutate(pool = group_indices(., conveyor_id, belt_install_datetime),
         pool_desc = paste0(conveyor_id, "-", format(belt_install_datetime, "%Y-%m-%d"))) %>%
  arrange(pool_desc) 

head(thickness_pooled)
```

### Accumulating throughput

```{r}
pool_utilisation <- calculate_belt_utilisation(thickness_pooled, util_tidy) 
head(pool_utilisation)
```


### Combining throughput with pooled thickness data

Join onto pooled thickness data, this will form the base wear data that we can fit regression lines to in order to get the wear rate of each pool.

```{r}
wear_data <- thickness_pooled %>%
  left_join(pool_utilisation, by = c("pool", "datetime" = "datetime_ending"))
saveRDS(wear_data, file = paste0(model_dir, "wear_data_combined.rds"))
head(wear_data)
```

### Removing tests from pools that predate movements

Some pools contain measurements that predate material movements by a significant amount of time, resulting in a bunch of points piled up on the left hand side of the thickness vs. utilisation charts. 

This is still needed even after we removed thickness tests that predate the earliest recorded movement for that equipment.
It's possible for the earliest movement for some equipment to predate the first thickness test in a pool, but for there to be a subsequent gap in movements that result in zero utilisation until later tests in the belt life. This is true for P700 (c_062), for example.

We will not investigate the cause here, but simply trim all but one measurement dates (retaining the latest) that have 0 cumulative tonnes.

```{r}
first_pool_datetime <- pool_utilisation %>%
  group_by(pool) %>%
  filter(Mt_total_cum == 0) %>%
  summarise(first_datetime = max(datetime_ending))

wear_data <- wear_data %>%
  left_join(first_pool_datetime, by = "pool") %>%
  filter(datetime >= first_datetime) %>%
  select(-first_datetime)
```

Finally, we filter out pools that have less than three measurement dates. 
We defer this step until the end, because trimming tests dates could potentially leave too little data for the pool to be useful, where it otherwise would've had more than three measurements.

```{r}
pool_remove <- wear_data %>%
  group_by(pool) %>%
  summarise(n_dates = n_distinct(datetime)) %>%
  filter(n_dates < 3) %>%
  pull(pool)

wear_data <- wear_data %>% 
  filter(!pool %in% pool_remove) 
```

## Calculating wear rate

We will create two tables of wear rate:

  * mean wear rate
  * worst-case wear rate
  
Mean wear rate in this instance refers to the slope of the regression line fit to the mean belt thickness (taken over all measurement positions) over time and throughput.

Worst-case wear rate is the slope of the steepest regression line in the collection of regression lines fit to each measurement position, i.e. the fastest wearing lateral position on the belt.

Both tables will include the R2 value of the regression and the standard error of the slope, for both time and throughput metrics.

```{r}
wear_rates_mean <- calculate_wear_rates_mean(wear_data)
head(wear_rates_mean)
```

```{r}
# We get a couple of warnings about perfect fits, these should be safe to ignore.
wear_rates_max <- suppressWarnings(calculate_wear_rates_max(wear_data))
head(wear_rates_max)
```

## Writing to file

Finally, we can join the two wear rates tables with the report attributes and save a learning table.

Here we also have logic for ensuring that belt strength is homogeneous within each pool.
Currently, by there appear to only be two pools where this is not already the case.

We will take the most commonly occurring strength, and in the event of a tie, take the minimum.

TODO: consider moving this logic further upstream.

```{r}
# helper function for getting modal value(s)
modes <- function(x) {
  ux <- unique(x)
  tab <- tabulate(match(x, ux))
  ux[tab == max(tab)]
}

# lookup table for % fines of each pool
# some results will be undefined, but these pools are excluded from 
# our final modelling set anyway
pool_fines <- pool_utilisation %>% 
  group_by(pool) %>% 
  summarise(perc_fines = max(Mt_fines_cum) / max(Mt_total_cum))

pool_attrs <- thickness_pooled %>%
  group_by(pool) %>%
  select(pool, report_id) %>%
  distinct() %>%
  left_join(select(rep_attr_tidy, report_id, belt_width_mm, 
                   belt_strength_kNpm, conveyor_duty, belt_speed_ms, 
                   belt_length_m, load_frequency, conveyor_id, drop_height_m),
            by = "report_id") %>%
  group_by(pool) %>%
  # next line is belt strength pool logic
  mutate(
    belt_strength_kNpm = ifelse(
      all(is.na(belt_strength_kNpm)),
      NA,
      min(modes(belt_strength_kNpm), na.rm = TRUE))
    ) %>%
  ungroup() %>%
  left_join(pool_fines, by = "pool") %>% 
  select(-report_id) %>%
  distinct()

# No pool should have more than one row in this table, because we formed pools such that all
# explanatory variables are homogeneous
valid_results <- pool_attrs %>%
  group_by(pool) %>%
  summarise(n = n()) %>%
  pull(n) %>%
  all(. == 1)

if (!valid_results) {
  stop("non-homogeneous pools exist!")
}

learn_table_mean <- wear_rates_mean %>%
  left_join(pool_attrs, by = "pool") %>%
  tibble::add_column(wear_type = "mean", .before = "metric") %>%
  drop_na()

learn_table_max <- wear_rates_max %>%
  left_join(pool_attrs, by = "pool") %>%
  tibble::add_column(wear_type = "max", .before = "metric") %>%
  drop_na()

learn_table <- bind_rows(learn_table_mean, learn_table_max)

# Recommend reading .rds file if using R to potential avoid ambiguities or type conversion mishaps in csv
saveRDS(learn_table, paste0(model_dir, "/model-data.rds"))
write.csv(learn_table, paste0(model_dir, "/model-data.csv"), row.names = FALSE)
```

## Results

To summarise, we have the following records in the modelling set:

```{r}
learn_table %>%
  group_by(wear_type, metric) %>%
  summarise(n_records = n())
```


```{r, include = FALSE}
# This code chunk renders a visual summary of each conveyor pool, and will fail if you attempt to run it directly,
# it is only meant to be run when knitting (through the Knit button or rmarkdown::render)
conveyors <- unique(wear_data$conveyor_id)
pools <- purrr::map(conveyors, ~ unique(filter(wear_data, conveyor_id == .x)$pool)) %>%
  set_names(conveyors)

out <- NULL

for (conveyor_id in names(pools)) {
  out <- c(out, paste0("### ", conveyor_id)) 
  for (pool_id in pools[[conveyor_id]]) {
    out <- c(out, knit_child("pool-summary-template.Rmd", envir = parent.frame()))
  }
}
```

`r paste(knit(text = out), collapse = "\n")`





