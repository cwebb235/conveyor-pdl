---
title: "Dealing with multiple splices on same belt lifetime"
output: html_notebook
---

```{r, message=FALSE, warning=FALSE}
library(dplyr)
library(ggplot2)
#library(viridis)
#library(purrr)
library(tidyr)

# directory containing pdl data
data_root <- "../../data/"

rep_attr_tidy <- readRDS(paste0(data_root, "rep_attr_tidy.rds"))
thickness_tidy <- readRDS(paste0(data_root, "thickness_tidy.rds"))
```


## Purpose

To visualise thickness tests results from belts with more than one measured splice during a belt life, and to specify an appopriate method of aggregating these to a single record


## Identifying relevant conveyors

Here we count how many reports are associated with a given conveyor and belt install date, and filter out those with less than 2:

```{r}
rep_attr_tidy %>%
  arrange(conveyor_id, report_id) %>%
  group_by(conveyor_id, belt_install_date) %>%
  filter(n() > 1) %>%
  summarise(n_reports = n())
```

Not all of these reports necessarily cover the same period of time, let's visualise the testing coverage of these reports over time.

Each subplot contains the history for each unique (conveyor_id, belt_install_date) pair:


```{r, fig.width = 7, fig.height = 15}
test_dates <- thickness_tidy %>%
  select(datetime, report_id) %>%
  unique

splice_coverage <- rep_attr_tidy %>%
  arrange(conveyor_id, report_id) %>%
  group_by(conveyor_id, belt_install_date) %>%
  filter(n() > 1) %>% 
  mutate(belt_life = paste(conveyor_id, belt_install_date)) %>%
  left_join(test_dates , by = "report_id") %>%
  ungroup

ggplot(splice_coverage, aes(x = datetime, y = report_id)) +
  geom_point(aes(colour = conveyor_id)) +
  facet_wrap(~ belt_life, scales = "free_y", ncol = 2) +
  theme(legend.position = "none")
```


Some initial findings:

* Not all splices have the same test coverage, with some splices containing a more complete history than others.
* There's likely a data error with conveyor c_096, for which two splices have a belt installation date one day apart.

We should assess how different the same belt life at different splices appear for all of these cases, but let's consider just one to begin with.
Taking c_003 installed at 2007-07-11, which has six reports (splices) as an example.

We can start by eyeballing how consistent the wear at each measurement position between reports is:

```{r, fig.width = 8, fig.height = 12}
target <- "c_003 2007-07-11"
report_ids <- splice_coverage %>%
  filter(belt_life == target) %>%
  pull(report_id) %>% unique()

thickness_subset <- thickness_tidy %>% 
  filter(report_id %in% report_ids)

p <- ggplot(thickness_subset, aes(x = datetime, y = thickness_mm, colour = report_id)) +
  geom_line() +
  facet_wrap(~ position, ncol = 3)

p
```


We can also consider simply the mean thickness at each test date for each report:

```{r}
mean_thicknesses <- thickness_subset %>%
  group_by(datetime, report_id) %>%
  summarise(mean_thickness = mean(thickness_mm)) %>% print
```

```{r}
mean_thicknesses %>%
  group_by(datetime) %>%
  summarise(std_dev = sd(mean_thickness))
```

Or calculate the standard deviation of thickness for each (test date, location):

```{r}
thickness_subset %>%
  group_by(datetime, position) %>%
  summarise(mean = mean(thickness_mm),
            std_dev = sd(thickness_mm))
```

In this case, the standard deviation is less than 1mm for nearly every position, which is likely comparable to or smaller than the standard error of the measurement process. Therefore in this instance, it would seem reasonable to simply merge data from all splices and perform linear regression on the combined history at each position. 





