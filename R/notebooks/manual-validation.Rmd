---
title: "P887 manual validation"
output: html_notebook
---

```{r, echo = FALSE, message = FALSE, warning = FALSE}
library(ggplot2)
library(dplyr)
library(tidyr)
library(readxl)
library(knitr)
library(stringr)
library(data.table)

# Root data directory
data_root <- "../../data/"
```

## Purpose

To validate the results for P887 (c_001), from raw data using a more manual approach on the raw data.

This notebook will allow for intermediate results to be more easily compared to other approaches, e.g. Excel.

## P887

### Thickness data

```{r}
# We want thickness in long form
thickness_mat <- read_excel(paste0(data_root, "belt-con-mon/Copy of P887 R.xls"),
                            sheet = "R", skip = 2, col_names = FALSE) %>% t()

thickness_num <- data.frame(thickness_mat[2:nrow(thickness_mat), ]) %>% 
  setNames(thickness_mat[1, ])

dates <- read_excel(paste0(data_root, "belt-con-mon/Copy of P887 R.xls"),
                        sheet = "R", skip = 1, n_max = 1, col_names = FALSE)

thickness <- cbind(data.frame(date = t(dates[1, 2:ncol(dates)]),
                              stringsAsFactors = FALSE), 
                   thickness_num) %>%
  gather("position", "thickness", -date) %>%
  mutate(datetime = as.POSIXct(paste0(date, " 12:00:00"), tz = "UTC")) %>%
  select(datetime, position, thickness)
```


### Utilisation data

Start with the complete raw set, and we'll manually filter down to our conveyor.

```{r}
mt <- fread(paste0(data_root, "/material-tracking/MT Query for Tonnes by Belt_August 2014 Update.csv"), data.table = FALSE) %>%
  transmute(product_group = `Product Group`,
            product_name = ifelse(PRDCT_NAME == "", NA, PRDCT_NAME),
            start_datetime = as.POSIXct(START_DT, format = "%d/%m/%Y %H:%M:%S", tz = "UTC"),
            end_datetime = as.POSIXct(END_DT, format = "%d/%m/%Y %H:%M:%S", tz = "UTC"),
            equipment_list = ifelse(RSS_EQIDLIST == "", NA, toupper(RSS_EQIDLIST)),
            tonnes = TONNES,
            hours = as.numeric(end_datetime - start_datetime, units = "hours"))

# mt <- data.table::fread(paste0(data_root, "/material-tracking/MT Query for Tonnes by Belt_August 2014 Update.csv"), data.table = FALSE) %>%
#     transmute(product_group = `Product Group`,
#               product_name = ifelse(PRDCT_NAME == "", NA, PRDCT_NAME),
#               start_datetime = as.POSIXct(START_DT, format = "%d/%m/%Y %H:%M:%S"),
#               end_datetime = as.POSIXct(END_DT, format = "%d/%m/%Y %H:%M:%S"),
#               equipment_list = ifelse(RSS_EQIDLIST == "", NA, toupper(RSS_EQIDLIST)),
#               tonnes = TONNES) %>%
#     filter(dplyr::row_number() != 159307,
#            !grepl("ORA-20001: Receive Timeout", equipment_list)) %>% #, # remove known faulty rows
#            #tonnes / as.numeric(end_datetime - start_datetime, units = "hours"))
#     filter(!is.na(equipment_list),
#            equipment_list != "")
```

### Trimming thickness

With the raw utilisation set, we want to trim any leading thickness tests thate predate the earliest movement for our conveyor

```{r}
earliest_mvmnt <- mt %>%
  filter(grepl("P887", equipment_list)) %>%
  pull(start_datetime) %>%
  min()

thickness_trimmed <- thickness %>% 
  filter(datetime >= earliest_mvmnt)
```

### Subsetting movements

```{r}
mt_subset <- mt %>%
  filter(start_datetime >= min(thickness_trimmed$datetime),
         start_datetime < max(thickness_trimmed$datetime),
         grepl("P887", equipment_list),
         hours > 0,
         tonnes / hours > 300,
         tonnes / hours < 15000)
```


### Accumulating tonnes

```{r}
test_dtms <- unique(thickness_trimmed$datetime) %>% sort

tonnes_cum <- purrr::map_dbl(test_dtms, function(dtm) {
  mt_subset %>%
    filter(start_datetime <= dtm) %>%
    pull(tonnes) %>% sum
}) %>% setNames(format(test_dtms, "%Y-%m-%d"))

tonnes_cum
```

### Performing regression

We'll focus on worst case throughput wear rate.

```{r}
tonnes_cum_df <- data.frame(datetime = test_dtms,
                            mt_cum = tonnes_cum / 1E6)
```

```{r}
wear_data <- thickness_trimmed %>%
  left_join(tonnes_cum_df, by = "datetime")

wear_fits <- wear_data %>%
  group_by(position) %>%
  do(#fit_time = lm(thickness ~ weeks, data = .),
     fit_tonnes = lm(thickness ~ mt_cum, data = .))

wear_fit_max <- wear_fits %>%
  rowwise() %>%
  mutate(coef_name = names(fit_tonnes$coefficients)[[2]],
         rate = -1 * fit_tonnes$coefficients[[coef_name]],
         r2 = broom::glance(fit_tonnes)$r.squared,
         std_err = broom::tidy(fit_tonnes) %>%
         filter(term == coef_name) %>%
         pull(std.error)) %>%
  select(position, rate, r2, std_err) %>%
  ungroup() %>%
  arrange(desc(rate)) %>%
  head(1)

wear_fit_max
```

## P010 c005

### Thickness data

```{r}
# We want thickness in long form
thickness_mat <- read_excel(paste0(data_root, "belt-con-mon/Copy of P010 R.xls"),
                            sheet = "R", skip = 2, col_names = FALSE) %>% t()

thickness_num <- data.frame(thickness_mat[2:nrow(thickness_mat), ]) %>% 
  setNames(thickness_mat[1, ])

dates <- read_excel(paste0(data_root, "belt-con-mon/Copy of P010 R.xls"),
                        sheet = "R", skip = 1, n_max = 1, col_names = FALSE)

thickness <- cbind(data.frame(date = t(dates[1, 2:ncol(dates)]),
                              stringsAsFactors = FALSE), 
                   thickness_num) %>%
  gather("position", "thickness", -date) %>%
  mutate(datetime = as.POSIXct(paste0(date, " 12:00:00"), tz = "UTC"),
         position = as.numeric(position)) %>%
  select(datetime, position, thickness) %>%
  filter(position > 400, position < 1600 - 400 )
```

### Trimming thickness

With the raw utilisation set, we want to trim any leading thickness tests thate predate the earliest movement for our conveyor

```{r}
earliest_mvmnt <- mt %>%
  filter(grepl("P10,", equipment_list)) %>%
  pull(start_datetime) %>%
  min()

thickness_trimmed <- thickness %>% 
  filter(datetime >= earliest_mvmnt)
```

### Subsetting movements

```{r}
mt_subset <- mt %>%
  filter(start_datetime >= min(thickness_trimmed$datetime),
         start_datetime < max(thickness_trimmed$datetime),
         grepl("P10,", equipment_list),
         hours > 0,
         tonnes / hours > 300,
         tonnes / hours < 15000)
```


### Accumulating tonnes

```{r}
test_dtms <- unique(thickness_trimmed$datetime) %>% sort

tonnes_cum <- purrr::map_dbl(test_dtms, function(dtm) {
  mt_subset %>%
    filter(start_datetime <= dtm) %>%
    pull(tonnes) %>% sum
}) %>% setNames(format(test_dtms, "%Y-%m-%d"))

tonnes_cum
```

### Performing regression

We'll focus on worst case throughput wear rate.

```{r}
tonnes_cum_df <- data.frame(datetime = test_dtms,
                            mt_cum = tonnes_cum / 1E6)
```

```{r}
wear_data <- thickness_trimmed %>%
  left_join(tonnes_cum_df, by = "datetime")

wear_fits <- wear_data %>%
  group_by(position) %>%
  do(#fit_time = lm(thickness ~ weeks, data = .),
     fit_tonnes = lm(thickness ~ mt_cum, data = .))

wear_fit_max <- wear_fits %>%
  rowwise() %>%
  mutate(coef_name = names(fit_tonnes$coefficients)[[2]],
         rate = -1 * fit_tonnes$coefficients[[coef_name]],
         r2 = broom::glance(fit_tonnes)$r.squared,
         std_err = broom::tidy(fit_tonnes) %>%
         filter(term == coef_name) %>%
         pull(std.error)) %>%
  select(position, rate, r2, std_err) %>%
  ungroup() %>%
  arrange(desc(rate)) %>%
  head(1)

wear_fit_max
```









