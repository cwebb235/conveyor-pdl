---
#
# IMPORTANT 
# This is not a self-contained notebook, it is a child of 
# modelling-data-preparation.Rmd
#
# The parent notebook uses this as a templated for summarising 
# pooled data.
#

title: "Untitled"
output: html_document
---

```{r, include = FALSE}
pool_wear <- wear_data %>% filter(pool == pool_id)
```


#### Pool `r pool_id`

Data from the following report(s):

```{r, include = FALSE}
files <- pool_wear %>%
  select(report_id) %>%
  distinct() %>%
  left_join(rep_attr_tidy, by = "report_id") %>%
  pull(file) %>%
  str_split(., "/") %>% 
  purrr::map_chr(~tail(., 1))
```


`r paste0("\u0060", paste0(files, collapse = "\u0060, \u0060"), "\u0060")`

```{r, include = FALSE}
## Cross section plot

max_wear_pos <- wear_rates_max %>%
  filter(pool == pool_id) %>%
  pull(position) %>% head(1)

p_data <- pool_wear %>%
  mutate(line_group = group_indices(., report_id, datetime))

p_section <- ggplot(p_data, aes(x = position, y = thickness_mm, colour = weeks, group = line_group)) +
  geom_vline(xintercept = max_wear_pos, colour = "gray40") +
  geom_line() +
  scale_colour_viridis_c() +
  scale_y_continuous(name = "thickness (mm)", limits = c(0, NA)) +
  theme_bw() +
  ggtitle("cross section") +
  theme(panel.border = element_blank(),
        legend.position = "bottom")
```


```{r, echo = FALSE, fig.width = 8, fig.height = 3.2}
p_data <- pool_utilisation %>%
  filter(pool == pool_id,
         datetime_ending >= min(pool_wear$datetime)) %>%
  select(datetime_ending, weeks, Mt_fines_cum, Mt_lump_cum) %>%
  gather(material, Mt, Mt_fines_cum, Mt_lump_cum)

test_dates <- pool_wear %>%
  select(datetime, weeks) %>%
  distinct()

p_util <- ggplot(p_data, aes(x = datetime_ending, y = Mt, fill = material)) +
  geom_vline(data = test_dates, aes(xintercept = datetime, colour = weeks),
             alpha = 0.8) +
  geom_area(alpha = 0.75) +
  scale_x_datetime(name = "date") +
  scale_y_continuous(name = "cumulative MT") +
  #scale_fill_discrete(labels = c("fines", "lump")) +
  scale_fill_brewer(type = "qual", palette = "Paired",
                    labels = c("fines", "lump")) +
  scale_colour_viridis_c() +
  guides(colour = "none") +
  theme_bw() +
  ggtitle("cumulative throughput") +
  theme(panel.border = element_blank(),
        legend.position = "bottom",
        panel.grid.major.x = element_blank(),
        panel.grid.minor.x = element_blank())

plot_grid(p_section, p_util, ncol = 2, align = "h")
```


```{r, echo = FALSE, fig.width = 8, fig.height = 2.5}
# mean wear rate plot
p_util <- pool_utilisation %>%
  filter(pool == pool_id)

p_data <- pool_wear %>%
  group_by(datetime) %>%
  summarise(thickness_mean = mean(thickness_mm, na.rm = TRUE)) %>%
  left_join(p_util, by = c("datetime" = "datetime_ending"))

wear_time_summary <- filter(wear_rates_mean, pool == pool_id, metric == "mm/week")
wear_util_summary <- filter(wear_rates_mean, pool == pool_id, metric == "mm/MT")

time_text <- data.frame(x = weighted.mean(c(min(p_data$datetime), max(p_data$datetime)),
                                          w = c(5, 1)),
                        y = max(p_data$thickness_mean) * 0.25,
                        lab = paste0("atop(rate == ", round(wear_time_summary$rate, 3),
                                     ",R^2 == ", round(wear_time_summary$r2, 2), ")"))

util_text <- data.frame(x = weighted.mean(c(min(p_data$Mt_total_cum), max(p_data$Mt_total_cum)),
                                          w = c(5, 1)),
                        y = max(p_data$thickness_mean) * 0.25,
                        lab = paste0("atop(rate == ", round(wear_util_summary$rate, 3),
                                     ",R^2 == ", round(wear_util_summary$r2, 2), ")"))

p_mean_time <- ggplot(p_data, aes(x = datetime, y = thickness_mean)) +
  geom_point() +
  geom_smooth(method = "lm") +
  annotate("text", x = time_text$x, y = time_text$y,
           label = time_text$lab, parse = TRUE) +
  scale_y_continuous(name = "mean thickness (mm)", limits = c(0, NA)) +
  theme_bw() +
  ggtitle("mean thickness vs. time",
          "(thickness averaged over width)") +
  theme(panel.border = element_blank(),
        legend.position = "bottom",
        axis.title.x = element_blank(),
        axis.title.y = element_blank())

p_mean_util <- ggplot(p_data, aes(x = Mt_total_cum, y = thickness_mean)) +
  geom_point() +
  geom_smooth(method = "lm") +
  annotate("text", x = util_text$x, y = util_text$y,
           label = util_text$lab, parse = TRUE) +
  theme_bw() +
  scale_x_continuous(name = "cumulative Mt") +
  scale_y_continuous(limits = c(0, NA)) +
  ggtitle("mean thickness vs. throughput",
          "(thickness averaged over width)") +
  theme(panel.border = element_blank(),
        legend.position = "bottom",
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks.y = element_blank())

plot_grid(p_mean_time, p_mean_util, ncol = 2, align = "h")
```


```{r, echo = FALSE, fig.width = 8, fig.height = 2.5}
# max wear rate plot
wear_max <- wear_rates_max %>%
  filter(pool == pool_id)

wear_max_util <- filter(wear_max, metric == "mm/MT")
wear_max_time <- filter(wear_max, metric == "mm/week")

max_pos <- wear_max_util$position 

p_data <- wear_data %>%
  filter(pool == pool_id, position == max_pos)

time_text <- data.frame(x = weighted.mean(c(min(p_data$datetime), max(p_data$datetime)),
                                          w = c(5, 1)),
                        y = max(p_data$thickness_mm) * 0.25,
                        lab = paste0("atop(rate == ", round(wear_max_time$rate, 3),
                                     ",R^2 == ", round(wear_max_time$r2, 2), ")"))

util_text <- data.frame(x = weighted.mean(c(min(p_data$Mt_total_cum), max(p_data$Mt_total_cum)),
                                          w = c(5, 1)),
                        y = max(p_data$thickness_mm) * 0.25,
                        lab = paste0("atop(rate == ", round(wear_max_util$rate, 3),
                                     ",R^2 == ", round(wear_max_util$r2, 2), ")"))

p_max_time <- ggplot(p_data, aes(x = datetime, y = thickness_mm)) +
  geom_point() +
  geom_smooth(method = "lm") +
  annotate("text", x = time_text$x, y = time_text$y,
           label = time_text$lab, parse = TRUE) +
  scale_y_continuous(name = "thickness (mm)", limits = c(0, NA)) +
  theme_bw() +
  ggtitle(paste0("thickness vs. time at pos ", max_pos),
          "(worst case wear rate)") +
  theme(panel.border = element_blank(),
        legend.position = "bottom",
        axis.title.x = element_blank(),
        axis.title.y = element_blank())

p_max_util <- ggplot(p_data, aes(x = Mt_total_cum, y = thickness_mm)) +
  geom_point() +
  geom_smooth(method = "lm") +
  annotate("text", x = util_text$x, y = util_text$y,
           label = util_text$lab, parse = TRUE) +
  theme_bw() +
  scale_x_continuous(name = "cumulative Mt") +
  scale_y_continuous(limits = c(0, NA)) +
  ggtitle(paste0("thickness vs. throughput at pos ", max_pos),
          "(worst case wear rate)") +
  theme(panel.border = element_blank(),
        legend.position = "bottom",
        axis.title.y = element_blank(),
        axis.text.y = element_blank(),
        axis.ticks.y = element_blank())

plot_grid(p_max_time, p_max_util, ncol = 2, align = "h")
```



